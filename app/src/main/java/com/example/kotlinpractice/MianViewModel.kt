package com.example.kotlinpractice

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MianViewModel:ViewModel() {

    var count =0

    val currentNumber: MutableLiveData<Int> by lazy{
        MutableLiveData<Int>()
    }
}
package com.example.kotlinpractice

import android.Manifest
import android.annotation.SuppressLint
import android.app.TaskStackBuilder.create
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.LocationManager
import android.location.LocationRequest
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinpractice.databinding.FragmentHomeBinding
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import java.net.URI.create


class Home : BaseFragment<FragmentHomeBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding
        get() = FragmentHomeBinding ::inflate


    lateinit var viewModel: MianViewModel
    lateinit var PERMISSIONS: Array<out String>
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager
    override fun init() {

        viewModel = ViewModelProvider(requireActivity()).get(MianViewModel::class.java)
        viewModel.currentNumber.observe(requireActivity(),Observer{
            bi.countNUm.text = it.toString()
        })
        PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION

        )
        if (!hasPermissions(requireContext(), *PERMISSIONS)) {

            ActivityCompat.requestPermissions(requireActivity(),PERMISSIONS,1);
        }

        bi.btn1.setOnClickListener(){
            bi.text.setText("This is me!")
        }
        bi.btn2.setOnClickListener(){
            bi.text.setText("This is me!!")
        }
        bi.btn3.setOnClickListener(){
            bi.text.setText("This is me!!!")
        }
          bi.count.setOnClickListener(){
           viewModel.currentNumber.value = ++ viewModel.count

        }
        if (checkGPSEnabled() == true){
            turnOnGPS()
        }else if (checkGPSEnabled() == false){
            getCurrentLocation()
        }


    }
    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        // checking location permission
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // getting the last known or current location

//
                location.latitude
                location.longitude
//
//
                Log.d("latitude", "Latitude = " + location.latitude)
                Log.d("longitude", "longitude = " + location.longitude)
                // Log.d("latitude", "Longitude = " + location.longitude)
                // btOpenMap.visibility = View.VISIBLE
            }
            .addOnFailureListener {
                Toast.makeText(
                    requireContext(), "Failed on getting current location",
                    Toast.LENGTH_SHORT
                ).show()

            }
    }
    private fun hasPermissions(context: Context?, vararg PERMISSIONS: String): Boolean {
        if (context != null && PERMISSIONS != null) {
            for (permission in PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }
    private fun checkGPSEnabled(): Boolean {
        val manager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER).not()
    }
    private fun turnOnGPS() {
        val request = com.google.android.gms.location.LocationRequest.create().apply {

            interval = 4000
            priority = com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
            fastestInterval = 2000
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(request).build()
        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder)
        task.addOnFailureListener {
            if (it is ResolvableApiException) {
                try {
                    it.startResolutionForResult(requireActivity(), 12345)
                } catch (sendEx: IntentSender.SendIntentException) {
                }
            }
        }.addOnSuccessListener {

        }

    }

}